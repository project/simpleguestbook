
Drupal.CallbackAttachPost = function (data) {
  data = Drupal.parseJson(data);
  //$('#attach-wrapper').html('');
  // Place HTML into temporary div
  var div = document.createElement('div');
  var pid = data.pid
  $(div).html(data.data);
  $(div).hide();
  $('#simpleguestbook-wrapper-' + pid ).append(div);
  $(div).fadeIn('slow');
  Drupal.LinksAjaxLoad();
}

Drupal.LinksAjaxLoad = function() {

  $('a.readanswer').click(function () {
     $('#' + this.id).hide();
     $.ajax({
       type: "GET",
       url: "/simpleguestbook/ajax_load/" + this.id,
       success: Drupal.CallbackAttachPost
     });
     return false;
  })
}

// Global killswitch
if (Drupal.jsEnabled) {
    $(document).ready(Drupal.LinksAjaxLoad);
  }
